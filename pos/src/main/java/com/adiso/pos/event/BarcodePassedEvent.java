package com.adiso.pos.event;

import com.adiso.pos.application.Event;
import com.adiso.pos.application.EventBus;
import com.adiso.pos.scan.Barcode;

/**
 * @author asobiczewski
 */
public class BarcodePassedEvent implements Event {

    private Barcode barcode;

    public BarcodePassedEvent(Barcode barcode) {
        this.barcode = barcode;
    }

    public Barcode getBarcode() {
        return barcode;
    }

    @Override
    public void handle(EventBus eventBus) {
        eventBus.onEvent(this);
    }
}
