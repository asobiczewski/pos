package com.adiso.thirdparty;

/**
 * @author asobiczewski
 */
public interface VendorLCDDisplay {

    public void updateFirstLine(String value);

    public void updateSecondLine(String value);

    void print();
}
