package com.adiso.pos.event;

import com.adiso.pos.application.Event;
import com.adiso.pos.application.EventBus;

/**
 * @author asobiczewski
 */
public class ProductFailedToFindEvent implements Event {
    @Override
    public void handle(EventBus eventBus) {
        eventBus.onEvent(this);
    }
}
