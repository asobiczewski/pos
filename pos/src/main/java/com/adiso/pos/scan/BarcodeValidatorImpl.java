package com.adiso.pos.scan;

import com.adiso.pos.error.BarcodeNotValidException;

/**
 * @author asobiczewski
 */
public class BarcodeValidatorImpl implements BarcodeValidator {

    public BarcodeValidatorImpl() {}

    @Override
    public Barcode validate(String value) throws BarcodeNotValidException {
        if (!valid(value))
            throw new BarcodeNotValidException();
        return new Barcode(value);
    }

    private boolean valid(String s) {
        return s != null && !"".equalsIgnoreCase(s);
    }
}
