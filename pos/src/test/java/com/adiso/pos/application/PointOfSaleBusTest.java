package com.adiso.pos.application;

import com.adiso.pos.error.BarcodeNotValidException;
import com.adiso.pos.event.*;
import com.adiso.pos.output.Display;
import com.adiso.pos.output.ReceiptPrinter;
import com.adiso.pos.sales.Order;
import com.adiso.pos.sales.OrderService;
import com.adiso.pos.sales.Product;
import com.adiso.pos.scan.Barcode;
import com.adiso.pos.scan.BarcodeValidator;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

/**
 * @author asobiczewski
 */
public class PointOfSaleBusTest {

    public static final String BARCODE_VALUE = "10";

    private PointOfSaleBus pointOfSaleBus;
    private Display display;
    private ReceiptPrinter reciptPrinter;
    private BarcodeValidator barcodeValidator;
    private OrderService orderService;
    private Barcode barcode;

    @BeforeMethod
    public void setUp() throws Exception {
        display = mock(Display.class);
        reciptPrinter = mock(ReceiptPrinter.class);
        barcodeValidator = mock(BarcodeValidator.class);
        orderService = mock(OrderService.class);
        pointOfSaleBus = spy(new PointOfSaleBus(display, reciptPrinter, barcodeValidator, orderService));
        barcode = new Barcode(BARCODE_VALUE);
    }

    @Test
    public void onEventBarcodeScannedEvent_validBarcode_firesBarcodePassedEvent() throws Exception {
        when(barcodeValidator.validate(any(String.class))).thenReturn(barcode);
        pointOfSaleBus.onEvent(new BarcodeScannedEvent(BARCODE_VALUE));
        verify(pointOfSaleBus, atLeastOnce()).onEvent(any(BarcodePassedEvent.class));
    }

    @Test
    public void onEventBarcodeScannedEvent_inValidBarcode_firesBarcodeRejectEvent() throws Exception {
        when(barcodeValidator.validate(any(String.class))).thenThrow(new BarcodeNotValidException());
        pointOfSaleBus.onEvent(new BarcodeScannedEvent(BARCODE_VALUE));
        verify(pointOfSaleBus, atLeastOnce()).onEvent(any(BarcodeRejectedEvent.class));
    }

    @Test
    public void onEventBarcodePassedEvent_Exit() throws Exception {
        pointOfSaleBus.onEvent(new BarcodePassedEvent(new Barcode("exit")));
        verify(orderService, atLeastOnce()).submit(any(Order.class));
    }

    @Test
    public void onEventBarcodePassedEvent_NormalBarcode() throws Exception {
        pointOfSaleBus.onEvent(new BarcodePassedEvent(barcode));
        verify(orderService, atLeastOnce()).addProduct(any(Order.class), any(Barcode.class));
    }

    @Test
    public void onEventBarcodeRejectedEvent() throws Exception {
        pointOfSaleBus.onEvent(new BarcodeRejectedEvent());
        verify(display, atLeastOnce()).errorInvalidBarcode();
    }

    @Test
    public void onEventProductAddedToOrderEvent() throws Exception {
        pointOfSaleBus.onEvent(new ProductAddedToOrderEvent(new Product("Product", 10)));
        verify(display, atLeastOnce()).display(any(Product.class));
    }

    @Test
    public void onEventProductFailedToFindEvent() throws Exception {
        pointOfSaleBus.onEvent(new ProductFailedToFindEvent());
        verify(display, atLeastOnce()).errorProductNotFound();
    }

    @Test
    public void onEventOrderSubmittedEvent() throws Exception {
        pointOfSaleBus.onEvent(new OrderSubmittedEvent(new Order()));
        verify(reciptPrinter, atLeastOnce()).printReceipt(any(Order.class));
    }
}
