package com.adiso.pos.scan;

import com.adiso.pos.error.BarcodeNotValidException;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 * @author asobiczewski
 */
public class BarcodeValidatorImplTest {

    private BarcodeValidator barcodeValidator;

    @BeforeMethod
    public void setUp() throws Exception {
        barcodeValidator = new BarcodeValidatorImpl();
    }

    @Test(expectedExceptions = BarcodeNotValidException.class)
    public void validate_null_fails() throws Exception {
        barcodeValidator.validate(null);
    }

    @Test(expectedExceptions = BarcodeNotValidException.class)
    public void validate_emptyString_fails() throws Exception {
        barcodeValidator.validate("");
    }

    @Test
    public void validate_string_ok() throws Exception {
        barcodeValidator.validate("s");
    }

    @Test
    public void validate_number_ok() throws Exception {
        barcodeValidator.validate("30");
    }

}
