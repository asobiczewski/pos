package com.adiso.pos.event;

import com.adiso.pos.application.Event;
import com.adiso.pos.application.EventBus;

/**
 * @author asobiczewski
 */
public class BarcodeScannedEvent implements Event {

    private String scannedBarcode;

    public BarcodeScannedEvent(String scannedBarcode) {
        this.scannedBarcode = scannedBarcode;
    }

    public String getBarcode() {
        return scannedBarcode;
    }

    @Override
    public void handle(EventBus eventBus) {
        eventBus.onEvent(this);
    }
}
