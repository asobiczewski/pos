package com.adiso.thirdparty;

/**
 * @author asobiczewski
 */
public interface VendorReceiptPrinter {

    public void print(String text);

}
