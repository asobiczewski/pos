package com.adiso.pos.scan;

import com.adiso.pos.error.BarcodeNotValidException;

/**
 * @author asobiczewski
 */
public interface BarcodeValidator {
    Barcode validate(String barcode) throws BarcodeNotValidException;
}
