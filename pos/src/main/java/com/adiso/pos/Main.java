package com.adiso.pos;

import com.adiso.pos.application.PointOfSaleBus;
import com.adiso.pos.event.BarcodeScannedEvent;
import com.adiso.pos.output.Display;
import com.adiso.pos.output.DisplayAdapter;
import com.adiso.pos.output.ReceiptPrinter;
import com.adiso.pos.output.ReceiptPrinterImpl;
import com.adiso.pos.sales.OrderService;
import com.adiso.pos.sales.OrderServiceImpl;
import com.adiso.pos.sales.ProductFinder;
import com.adiso.pos.sales.ProductFinderImpl;
import com.adiso.pos.scan.BarcodeValidator;
import com.adiso.pos.scan.BarcodeValidatorImpl;
import com.adiso.thirdparty.VendorLCDDisplay;
import com.adiso.thirdparty.VendorLCDDisplayImpl;
import com.adiso.thirdparty.VendorReceiptPrinter;
import com.adiso.thirdparty.VendorReceiptPrinterImpl;

/**
 * @author asobiczewski
 */
public class Main {
    
    public static void main(String[] args) {
        VendorLCDDisplay vendorLCDDisplay = new VendorLCDDisplayImpl();
        Display display = new DisplayAdapter(vendorLCDDisplay);
        VendorReceiptPrinter vendorReceiptPrinter = new VendorReceiptPrinterImpl();
        ReceiptPrinter receiptPrinter = new ReceiptPrinterImpl(vendorReceiptPrinter);
        BarcodeValidator barcodeValidator = new BarcodeValidatorImpl();
        ProductFinder productFinder = new ProductFinderImpl();
        OrderService orderService = new OrderServiceImpl(productFinder);
        PointOfSaleBus pointOfSaleBus = new PointOfSaleBus(display, receiptPrinter, barcodeValidator, orderService);

        pointOfSaleBus.post(new BarcodeScannedEvent("1"));
        pointOfSaleBus.post(new BarcodeScannedEvent("2"));
        pointOfSaleBus.post(new BarcodeScannedEvent(""));//invalid barcode
        pointOfSaleBus.post(new BarcodeScannedEvent("3"));
        pointOfSaleBus.post(new BarcodeScannedEvent("5"));// product not found
        pointOfSaleBus.post(new BarcodeScannedEvent("exit"));
    }
    
}

