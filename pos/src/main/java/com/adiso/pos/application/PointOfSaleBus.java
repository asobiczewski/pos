package com.adiso.pos.application;

import com.adiso.pos.error.BarcodeNotValidException;
import com.adiso.pos.event.*;
import com.adiso.pos.output.Display;
import com.adiso.pos.output.ReceiptPrinter;
import com.adiso.pos.sales.Order;
import com.adiso.pos.sales.OrderService;
import com.adiso.pos.scan.Barcode;
import com.adiso.pos.scan.BarcodeValidator;

/**
 * @author asobiczewski
 */
public class PointOfSaleBus implements EventBus {

    private Display display;
    private ReceiptPrinter receiptPrinter;
    private BarcodeValidator barcodeValidator;
    private OrderService orderService;
    private Order order = new Order();

    public PointOfSaleBus(Display display, ReceiptPrinter receiptPrinter, BarcodeValidator barcodeValidator, OrderService orderService) {
        this.display = display;
        this.receiptPrinter = receiptPrinter;
        this.barcodeValidator = barcodeValidator;
        this.orderService = orderService;
        orderService.setEventBus(this);
    }

    @Override
    public void onEvent(BarcodeScannedEvent barcodeScannedEvent) {
        try {
            Barcode barcode = barcodeValidator.validate(barcodeScannedEvent.getBarcode());
            post(new BarcodePassedEvent(barcode));
        } catch (BarcodeNotValidException e) {
            post(new BarcodeRejectedEvent());
        }
    }

    @Override
    public void onEvent(BarcodePassedEvent barcodePassedEvent) {
        Barcode barcode = barcodePassedEvent.getBarcode();
        if(barcode.isExitCommand())
            order = orderService.submit(order);
        else
            orderService.addProduct(order, barcode);
    }

    @Override
    public void onEvent(BarcodeRejectedEvent barcodeRejectedEvent) {
        display.errorInvalidBarcode();
    }

    @Override
    public void onEvent(ProductAddedToOrderEvent productAddedToOrderEvent) {
        display.display(productAddedToOrderEvent.getProduct());
    }

    @Override
    public void onEvent(ProductFailedToFindEvent productFailedToFindEvent) {
        display.errorProductNotFound();
    }

    @Override
    public void onEvent(OrderSubmittedEvent orderSubmittedEvent) {
        receiptPrinter.printReceipt(orderSubmittedEvent.getOrder());
    }

    @Override
    public void post(Event event) {
        event.handle(this);
    }
}
