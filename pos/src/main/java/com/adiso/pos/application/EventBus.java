package com.adiso.pos.application;

import com.adiso.pos.event.*;

/**
 * @author asobiczewski
 */
public interface EventBus {
    public void post(Event event);

    void onEvent(ProductFailedToFindEvent productFailedToFindEvent);

    void onEvent(OrderSubmittedEvent orderSubmittedEvent);

    void onEvent(BarcodeScannedEvent barcodeScannedEvent);

    void onEvent(BarcodePassedEvent barcodePassedEvent);

    void onEvent(BarcodeRejectedEvent barcodeRejectedEvent);

    void onEvent(ProductAddedToOrderEvent productAddedToOrderEvent);
}
