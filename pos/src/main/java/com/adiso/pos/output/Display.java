package com.adiso.pos.output;

import com.adiso.pos.sales.Product;

/**
 * @author asobiczewski
 */
public interface Display {
    void errorProductNotFound();

    void errorInvalidBarcode();

    void display(Product product);

}
