package com.adiso.pos.application;

/**
 * @author asobiczewski
 */
public interface Event {
    public void handle(EventBus eventBus);
}
