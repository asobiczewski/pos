package com.adiso.pos.sales;


import com.adiso.pos.error.ProductNotFoundException;
import com.adiso.pos.scan.Barcode;

/**
 * @author asobiczewski
 */
public class ProductFinderImpl implements ProductFinder {

    @Override
    public Product findBy(Barcode barcode) throws ProductNotFoundException {
        switch (barcode.getValue()){
            case "1":
                return new Product("Shoes", 100);
            case "2":
                return new Product("Juice", 10);
            case "3":
                return new Product("IceCream", 5);
            default:
                throw new ProductNotFoundException();
        }
    }

}
