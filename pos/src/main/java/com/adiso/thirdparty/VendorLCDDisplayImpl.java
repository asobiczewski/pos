package com.adiso.thirdparty;

/**
 * @author asobiczewski
 */
public class VendorLCDDisplayImpl implements VendorLCDDisplay {

    private String firstLine;
    private String secondLine;

    @Override
    public void updateFirstLine(String value) {
        this.firstLine = value;
    }

    @Override
    public void updateSecondLine(String value) {
        this.secondLine = value;
    }

    @Override
    public void print(){
        System.out.println("DISPLAY: ");
        System.out.println("##########");
        System.out.println(firstLine);
        System.out.println(secondLine);
        System.out.println("##########");
        System.out.println("\n");
    }
}
