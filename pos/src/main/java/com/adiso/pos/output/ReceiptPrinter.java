package com.adiso.pos.output;

import com.adiso.pos.sales.Order;

/**
 * @author asobiczewski
 */
public interface ReceiptPrinter {
    void printReceipt(Order order);
}
