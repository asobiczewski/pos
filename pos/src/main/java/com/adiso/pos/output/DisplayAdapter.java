package com.adiso.pos.output;

import com.adiso.pos.sales.Product;
import com.adiso.thirdparty.VendorLCDDisplay;

/**
 * @author asobiczewski
 */
public class DisplayAdapter implements Display {

    public static final String PRODUCT_NOT_FOUND = "Product not found";
    public static final String INVALID_BAR_CODE = "Invalid bar-code";
    public static final String PRODUCT_FORMAT = "%s .................... %s";
    public static final String TOTAL_FORMAT = "TOTAL: .................... %s";

    private VendorLCDDisplay vendorLCDDisplay;
    private double total = 0;

    public DisplayAdapter(VendorLCDDisplay vendorLCDDisplay) {
        this.vendorLCDDisplay = vendorLCDDisplay;
    }

    @Override
    public void errorProductNotFound() {
        this.vendorLCDDisplay.updateFirstLine(PRODUCT_NOT_FOUND);
        update();
    }

    @Override
    public void errorInvalidBarcode() {
        this.vendorLCDDisplay.updateFirstLine(INVALID_BAR_CODE);
        update();
    }

    @Override
    public void display(Product product) {
        this.total += product.getPrice();
        this.vendorLCDDisplay.updateFirstLine(String.format(PRODUCT_FORMAT, product.getName(), product.getPrice()));
        this.vendorLCDDisplay.updateSecondLine(String.format(TOTAL_FORMAT, total));
        update();
    }

    private void update() {
        this.vendorLCDDisplay.print();
    }

}
