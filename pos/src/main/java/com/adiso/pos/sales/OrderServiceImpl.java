package com.adiso.pos.sales;

import com.adiso.pos.application.EventBus;
import com.adiso.pos.error.ProductNotFoundException;
import com.adiso.pos.event.OrderSubmittedEvent;
import com.adiso.pos.event.ProductAddedToOrderEvent;
import com.adiso.pos.event.ProductFailedToFindEvent;
import com.adiso.pos.scan.Barcode;

/**
 * @author asobiczewski
 */
public class OrderServiceImpl implements OrderService {

    private EventBus eventBus;
    private ProductFinder productFinder;

    public OrderServiceImpl(ProductFinder productFinder) {
        this.productFinder = productFinder;
    }

    @Override
    public void setEventBus(EventBus eventBus) {
        this.eventBus = eventBus;
    }

    @Override
    public void addProduct(Order order, Barcode barcode) {
        try {
            Product product = productFinder.findBy(barcode);
            order.add(product);
            eventBus.post(new ProductAddedToOrderEvent(product));
        } catch (ProductNotFoundException e) {
            eventBus.post(new ProductFailedToFindEvent());
        }
    }

    @Override
    public Order submit(Order order) {
        eventBus.post(new OrderSubmittedEvent(order));
        return order.submit();
    }

}
