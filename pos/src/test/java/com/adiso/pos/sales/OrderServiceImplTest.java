package com.adiso.pos.sales;

import com.adiso.pos.application.EventBus;
import com.adiso.pos.error.ProductNotFoundException;
import com.adiso.pos.event.OrderSubmittedEvent;
import com.adiso.pos.event.ProductAddedToOrderEvent;
import com.adiso.pos.scan.Barcode;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;
import static org.testng.Assert.assertTrue;

/**
 * @author asobiczewski
 */
@SuppressWarnings("unchecked")
public class OrderServiceImplTest {

    private OrderServiceImpl orderService;
    private ProductFinder productFinder;
    private EventBus eventBus;
    private Product product;

    @BeforeMethod
    public void setUp() throws Exception {
        product = new Product("Product", 10);
        productFinder = mock(ProductFinder.class);
        when(productFinder.findBy(any(Barcode.class))).thenReturn(product);
        eventBus = mock(EventBus.class);
        orderService = new OrderServiceImpl(productFinder);
        orderService.setEventBus(eventBus);
    }

    @Test
    public void addProduct_Order_Product_OrderContainsProduct() throws Exception {
        Order order = new Order();
        orderService.addProduct(order, new Barcode(""));
        assertTrue(order.getProducts().contains(product));

    }

    @Test
    public void addProduct_Order_Product_FiresProductAddedEvent() throws Exception {
        orderService.addProduct(new Order(), new Barcode(""));
        verify(eventBus, atLeastOnce()).post(any(ProductAddedToOrderEvent.class));
    }

    @Test
    public void addProduct_Order_ProductNotExisting_FiresProductFailedToFind() throws Exception {
        when(productFinder.findBy(any(Barcode.class))).thenThrow(ProductNotFoundException.class);
        orderService.addProduct(new Order(), new Barcode(""));
        verify(eventBus, atLeastOnce()).post(any(ProductAddedToOrderEvent.class));
    }

    @Test
    public void submit_firesOrderSubmittedEvent() throws Exception {
        orderService.addProduct(new Order(), new Barcode(""));
        verify(eventBus, atLeastOnce()).post(any(OrderSubmittedEvent.class));
    }

}
