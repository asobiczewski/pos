package com.adiso.pos.event;

import com.adiso.pos.application.Event;
import com.adiso.pos.application.EventBus;
import com.adiso.pos.sales.Product;

/**
 * @author asobiczewski
 */
public class ProductAddedToOrderEvent implements Event {

    private Product product;

    public ProductAddedToOrderEvent(Product product) {
        this.product = product;
    }

    public Product getProduct() {
        return product;
    }


    @Override
    public void handle(EventBus eventBus) {
        eventBus.onEvent(this);
    }
}
