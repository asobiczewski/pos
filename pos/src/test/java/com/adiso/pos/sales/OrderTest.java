package com.adiso.pos.sales;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;


/**
 * @author asobiczewski
 */
public class OrderTest {

    private Order order;

    @BeforeMethod
    public void setUp() throws Exception {
        order = new Order();
    }

    @Test
    public void afterInitialization_TotalEqualsTo_0() throws Exception {
        assertEquals(order.getTotal(), 0.0);
    }

    @Test(expectedExceptions = IllegalArgumentException.class)
    public void addProduct_null_throwsException() throws Exception {
        order.add(null);
    }

    @Test
    public void addProduct_product_increaseTotal() throws Exception {
        order.add(new Product("Name", 30.0));
        assertEquals(order.getTotal(), 30.0);
    }
}
