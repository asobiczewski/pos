package com.adiso.pos.sales;


import com.adiso.pos.error.ProductNotFoundException;
import com.adiso.pos.scan.Barcode;

/**
 * @author asobiczewski
 */
public interface ProductFinder {
    public Product findBy(Barcode barcode) throws ProductNotFoundException;
}
