package com.adiso.pos.output;

import com.adiso.pos.sales.Order;
import com.adiso.pos.sales.Product;
import com.adiso.thirdparty.VendorReceiptPrinter;

/**
 * @author asobiczewski
 */
public class ReceiptPrinterImpl implements ReceiptPrinter {

    public static final String BREAK = "************************************\n";
    public static final String DOTS = "..........................";
    public static final String RECEIPT = "Receipt:\n";
    public static final String TOTAL = "TOTAL: ";
    public static final String NEXT_LINE = "\n";
    private VendorReceiptPrinter vendorReceiptPrinter;

    public ReceiptPrinterImpl(VendorReceiptPrinter vendorReceiptPrinter) {
        this.vendorReceiptPrinter = vendorReceiptPrinter;
    }

    @Override
    public void printReceipt(Order order) {
        StringBuilder sb = new StringBuilder(RECEIPT);
        sb.append(BREAK);
        for (Product product : order.getProducts()) {
            sb.append(product.getName()).append(DOTS).append(product.getPrice()).append(NEXT_LINE);
        }
        sb.append(BREAK)
                .append(TOTAL).append(order.getTotal());

        vendorReceiptPrinter.print(sb.toString());
    }

}
