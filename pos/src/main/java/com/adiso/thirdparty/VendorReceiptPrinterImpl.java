package com.adiso.thirdparty;

/**
 * @author asobiczewski
 */
public class VendorReceiptPrinterImpl implements VendorReceiptPrinter {

    @Override
    public void print(String text) {
        System.out.println(text);
    }
}
