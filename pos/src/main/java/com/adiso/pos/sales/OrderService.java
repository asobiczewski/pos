package com.adiso.pos.sales;

import com.adiso.pos.application.EventBus;
import com.adiso.pos.scan.Barcode;

/**
 * @author asobiczewski
 */
public interface OrderService {

    void setEventBus(EventBus eventBus);

    public void addProduct(Order order, Barcode barcode);

    public Order submit(Order order);
}
