package com.adiso.pos.event;

import com.adiso.pos.application.Event;
import com.adiso.pos.application.EventBus;
import com.adiso.pos.sales.Order;

/**
 * @author asobiczewski
 */
public class OrderSubmittedEvent implements Event {

    private Order order;

    public OrderSubmittedEvent(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }

    @Override
    public void handle(EventBus eventBus) {
        eventBus.onEvent(this);
    }
}
