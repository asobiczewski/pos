package com.adiso.pos.scan;

/**
 * @author asobiczewski
 */
public class Barcode {

    private final String value;

    public Barcode(String value) {
        this.value = value;
    }

    public String getValue() {
        return value != null ? value : "";
    }

    public boolean isExitCommand() {
        return "exit".equalsIgnoreCase(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Barcode barcode = (Barcode) o;

        return !(value != null ? !value.equals(barcode.value) : barcode.value != null);

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}
