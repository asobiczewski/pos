# NOTE: #

Main class with main method is provided only for visual purposes
so do implementation of findBy in ProductFinder.class

# ASSUMPTIONS: #

* Sales Person cannot adjust quantity of added product either by providing value or scanning twice
* Any Barcode value different than null & empty string is valid