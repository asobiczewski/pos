package com.adiso.pos.sales;

import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.Set;

/**
 * @author asobiczewski
 */
public class Order {

    private Set<Product> products;
    private double total = 0;

    public Order() {
        products = new LinkedHashSet<>();
        total = 0;
    }

    public void add(Product product) {
        if(product == null)
            throw new IllegalArgumentException("Product cannot be null");
        products.add(product);
        total += product.getPrice();
    }

    public Order submit() {
        return new Order();
    }

    public Set<Product> getProducts() {
        return Collections.unmodifiableSet(products);
    }

    public double getTotal() {
        return total;
    }
}
